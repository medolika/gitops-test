#!/bin/bash
TOKEN=$(oc whoami -t)
KUBE_API=$(oc config view -o jsonpath='{.clusters[0].cluster.server}')
NAMESPACE=openshift-gitops

curl -H "Authorization: Bearer $TOKEN" \
  -H "Content-Type: application/json-patch+json" \
  -H "Accept: application/json" \
  -X PATCH \
  --data '[{"op": "replace", "path": "/spec/source/targetRevision", "value": "ccb954c5e120a6c671bbb9a80fe8d88e02e53ceb"}]'  \
  --insecure \
  $KUBE_API/apis/argoproj.io/v1alpha1/namespaces/$NAMESPACE/applications/gitops-test


sleep 5

# To check if the revision is set right
curl -k  --header "Accept: application/vnd.kubernetes.protobuf, */*" -H "Authorization: Bearer $TOKEN" \
 -X GET $KUBE_API/apis/argoproj.io/v1alpha1/namespaces/$NAMESPACE/applications/gitops-test | grep -A3 deployStartedAt | tail



